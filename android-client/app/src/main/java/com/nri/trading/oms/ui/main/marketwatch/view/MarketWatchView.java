package com.nri.trading.oms.ui.main.marketwatch.view;

import com.nri.trading.oms.ui.base.MvpView;

/**
 * Created by rahulc on 21.1.16.
 */
public interface MarketWatchView extends MvpView {

    void populateMarketData();

}
