/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */

package com.nri.trading.oms;

/**
 * Created by rahulc on 8.1.16.
 */
public class Constants {
    public static final String JWT_TOKEN_KEY ="JWT_TOKEN";
    public static final String TRUE ="true";
    public static final String USER_LOGGED_IN_KEY ="LOGIN_STATUS";
    public static final String REST_END_POINT ="http://10.3.5.13:8080";
    public  static  final String GSON_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public  static  final String PREF_FILE_NAME = "OMS_PREF";
    public  static  final String NAVIGATION_MENU = "NAVIGATION_MENU";



}
