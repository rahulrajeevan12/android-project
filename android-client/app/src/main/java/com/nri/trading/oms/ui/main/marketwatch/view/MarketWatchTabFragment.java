package com.nri.trading.oms.ui.main.marketwatch.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.nri.trading.R;
import com.nri.trading.oms.data.model.MarketWatchMainData;
import com.nri.trading.oms.ui.main.marketwatch.adapter.MarketWatchListAdapter;
import com.nri.trading.oms.ui.main.marketwatch.presenter.MarketWatchFragmentPresenter;
import com.nri.trading.oms.util.InfiniteScrollListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by rahulc on 19.1.16.
 */
public class MarketWatchTabFragment extends Fragment implements MarketWatchFragmentView {
    ListView listView;
    Context context;

    @Inject
    MarketWatchFragmentPresenter marketWatchFragmentPresenter;

    MarketWatchMainData marketWatchMainData = new MarketWatchMainData();
    List<MarketWatchMainData> myMarketWatchMainData;
    MarketWatchListAdapter marketWatchListAdapter;

    EditText searchItem;

    public MarketWatchTabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.inner_fragments, container, false);
        context = container.getContext();

        //marketWatchFragmentPresenter.getMarketData();

        marketWatchMainData.setCompanyName("Primary Health Care pvt Ltd");
        marketWatchMainData.setCompanyShortName("PRY");
        marketWatchMainData.setHeadLeftValue("-3.415");
        marketWatchMainData.setHeadRightValue("1.25");
        marketWatchMainData.setHeadValue("53.154");
        myMarketWatchMainData = new ArrayList<>();
        for (int i = 0; i <= 10; i++)
            myMarketWatchMainData.add(marketWatchMainData);


        listView = (ListView) rootView.findViewById(R.id.equityOptionList);


        searchItem = (EditText) rootView.findViewById(R.id.searchItem);
        searchItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                marketWatchListAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        marketWatchListAdapter = new MarketWatchListAdapter(context, myMarketWatchMainData);
        listView.setAdapter(marketWatchListAdapter);
        listView.setOnScrollListener(new InfiniteScrollListener(5) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                Log.d("Scroll","Scroll");
                /*List<HashMap<String, String>> newData = loader.loadData();
                dataList.addAll(newData);
                adapter.notifyDataSetChanged();*/
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView cmpName, cmpShortName, hValue, rValue, lValue;
                cmpName = (TextView) view.findViewById(R.id.companyName);
                cmpShortName = (TextView) view.findViewById(R.id.companyShortName);
                hValue = (TextView) view.findViewById(R.id.headValue);
                rValue = (TextView) view.findViewById(R.id.headRightValue);
                lValue = (TextView) view.findViewById(R.id.headLeftValue);
                Intent intent = new Intent(getContext(), MarketWatchDetailsActivity.class);
                intent.putExtra("CompanyName", cmpName.getText());
                intent.putExtra("CompanyShortName", cmpShortName.getText());
                intent.putExtra("HeadValue", hValue.getText());
                intent.putExtra("HeadLValue", lValue.getText());
                intent.putExtra("HeadRValue", rValue.getText());
                intent.putExtra("LColor", lValue.getCurrentTextColor());
                intent.putExtra("RColor", rValue.getCurrentTextColor());
                getContext().startActivity(intent);
            }
        });
        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
    }

}

