package com.nri.trading.oms.data.model;

/**
 * Created by rahulc on 20.1.16.
 */
public class MarketWatchDetailsData {
    String eoCompanyName;
    String eoHeadValue;
    String eoRValue;
    String eoLValue;
    String eoExpire;
    String eoStrike;
    String eoCall;

    public MarketWatchDetailsData() {
    }

    public MarketWatchDetailsData(String eoCompanyName, String eoHeadValue, String eoRValue, String eoLValue, String eoExpire, String eoStrike, String eoCall) {
        this.eoCompanyName = eoCompanyName;
        this.eoHeadValue = eoHeadValue;
        this.eoRValue = eoRValue;
        this.eoLValue = eoLValue;
        this.eoExpire = eoExpire;
        this.eoStrike = eoStrike;
        this.eoCall = eoCall;
    }

    public String getEoCompanyName() {
        return eoCompanyName;
    }

    public void setEoCompanyName(String eoCompanyName) {
        this.eoCompanyName = eoCompanyName;
    }

    public String getEoHeadValue() {
        return eoHeadValue;
    }

    public void setEoHeadValue(String eoHeadValue) {
        this.eoHeadValue = eoHeadValue;
    }

    public String getEoRValue() {
        return eoRValue;
    }

    public void setEoRValue(String eoRValue) {
        this.eoRValue = eoRValue;
    }

    public String getEoLValue() {
        return eoLValue;
    }

    public void setEoLValue(String eoLValue) {
        this.eoLValue = eoLValue;
    }

    public String getEoExpire() {
        return eoExpire;
    }

    public void setEoExpire(String eoExpire) {
        this.eoExpire = eoExpire;
    }

    public String getEoStrike() {
        return eoStrike;
    }

    public void setEoStrike(String eoStrike) {
        this.eoStrike = eoStrike;
    }

    public String getEoCall() {
        return eoCall;
    }

    public void setEoCall(String eoCall) {
        this.eoCall = eoCall;
    }
}
