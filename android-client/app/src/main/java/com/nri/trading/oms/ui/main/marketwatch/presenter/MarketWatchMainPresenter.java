package com.nri.trading.oms.ui.main.marketwatch.presenter;

import com.nri.trading.oms.data.model.MarketWatchMainData;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by rahulc on 20.1.16.
 */
public class MarketWatchMainPresenter implements Presenter<MarketWatchView> {

    MarketWatchView marketWatchView;
    @Inject
    DataClient mDataClient;

    @Inject
    public MarketWatchMainPresenter() {
    }

    @Override
    public void attachView(MarketWatchView mvpView) {
        this.marketWatchView = mvpView;
    }

    @Override
    public void detachView() {
        this.marketWatchView = null;
    }

    public List<MarketWatchMainData> getMarketData() {
        MarketWatchMainData marketWatchMainData = new MarketWatchMainData();
        List<MarketWatchMainData> myMarketWatchMainData;
        marketWatchMainData.setCompanyName("Primary Health Care pvt Ltd");
        marketWatchMainData.setCompanyShortName("PRY");
        marketWatchMainData.setHeadLeftValue("-3.415");
        marketWatchMainData.setHeadRightValue("1.25");
        marketWatchMainData.setHeadValue("53.154");
        myMarketWatchMainData = new ArrayList<>();
        for (int i = 0; i <= 10; i++)
            myMarketWatchMainData.add(marketWatchMainData);

        return myMarketWatchMainData;

    }

}
