/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */

package com.nri.trading.oms.injection.component;

import com.nri.trading.oms.injection.PreActivity;
import com.nri.trading.oms.injection.module.ActivityModule;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchActivity;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchDetailsActivity;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchTabFragment;
import com.nri.trading.oms.ui.main.navigation.view.NavigationActivity;
import com.nri.trading.oms.ui.main.login.view.LoginActivity;

import dagger.Component;

/**
 * Created by rahulc on 14.1.16.
 */
@PreActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)

public interface ActivityComponent {

    void inject(LoginActivity loginActivity);
    void inject(NavigationActivity navigationActivity);
    void inject(MarketWatchActivity marketWatchActivity);
    void inject(MarketWatchTabFragment marketWatchTabFragment);
    void inject(MarketWatchDetailsActivity marketWatchDetailsActivity);


}
