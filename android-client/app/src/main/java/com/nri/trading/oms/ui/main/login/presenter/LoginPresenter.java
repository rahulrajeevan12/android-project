/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */

package com.nri.trading.oms.ui.main.login.presenter;

import com.nri.trading.oms.api.UserjwttokencontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.UserCredentials;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.login.view.LoginView;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by rahulc on 8.1.16.
 */
public class LoginPresenter implements Presenter<LoginView> {
    LoginView loginView;
    @Inject
    DataClient mDataClient;

    @Inject
    public LoginPresenter() {
    }

    public void doLogin(String userName, String password) {
        UserjwttokencontrollerApi authService = mDataClient.createService(UserjwttokencontrollerApi.class);
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setUserName(userName);
        userCredentials.setPassword(password);
        authService.authorizeUsingPOST(userCredentials, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                loginView.onLoginSuccess(true, s);
            }

            @Override
            public void failure(RetrofitError error) {
                loginView.onLoginFailure(error);
            }
        });
    }


    @Override
    public void attachView(LoginView mvpView) {
        this.loginView = mvpView;
    }

    @Override
    public void detachView() {
        this.loginView = null;
    }
}
