package com.nri.trading.oms.ui.main.marketwatch.view;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.daimajia.swipe.util.Attributes;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.nri.trading.R;
import com.nri.trading.oms.data.model.MarketWatchDetailsData;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.marketwatch.adapter.MarketWatchDetailsListAdapter;

import java.util.ArrayList;
import java.util.List;

public class MarketWatchDetailsActivity extends BaseActivity {
    TextView companyName;
    TextView companyShortName;
    TextView headValue;
    TextView lValue;
    TextView rValue;
    private RecyclerView mRecyclerView;
    private MarketWatchDetailsListAdapter marketWatchDetailsListAdapter;

    BottomSheetLayout bottomSheetLayout;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_watch_details);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.market_watch_toolbar);
        setSupportActionBar(mToolbar);

        initialValue();
        bottomSheetLayout = (BottomSheetLayout) findViewById(R.id.bottom_sheet_parent_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_stock_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        marketWatchDetailsListAdapter = new MarketWatchDetailsListAdapter(this, getData(), bottomSheetLayout);
        mRecyclerView.setAdapter(marketWatchDetailsListAdapter);
        marketWatchDetailsListAdapter.setMode(Attributes.Mode.Single);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    public static List<MarketWatchDetailsData> getData() {
        List<MarketWatchDetailsData> marketWatchDetailsDataList = new ArrayList<>();
        MarketWatchDetailsData marketWatchDetailsData = new MarketWatchDetailsData();
        marketWatchDetailsData.setEoCompanyName("Primary Health Care pvt Ltd");
        marketWatchDetailsData.setEoCall("Call");
        marketWatchDetailsData.setEoStrike("2015");
        marketWatchDetailsData.setEoHeadValue("50.52");
        marketWatchDetailsData.setEoLValue("2.01");
        marketWatchDetailsData.setEoRValue("52.5");
        marketWatchDetailsData.setEoExpire("15/12/2015");

        for (int index = 0; index <= 10; index++) {
            marketWatchDetailsDataList.add(marketWatchDetailsData);
        }
        return marketWatchDetailsDataList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_equity_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.setting_equity_option) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void initialValue() {
        Bundle companyDetails = getIntent().getExtras();
        companyName = (TextView) findViewById(R.id.eqOptionCompanyName);
        companyShortName = (TextView) findViewById(R.id.eqOptionCompanyShortName);
        headValue = (TextView) findViewById(R.id.eqOptionHeadValue);
        lValue = (TextView) findViewById(R.id.eqOPtionLvalue);
        rValue = (TextView) findViewById(R.id.eqOptionRvalue);
        companyName.setText(companyDetails.getString("CompanyName"));
        companyShortName.setText(companyDetails.getString("CompanyShortName"));
        headValue.setText(companyDetails.getString("HeadValue"));
        lValue.setText(companyDetails.getString("HeadLValue"));
        rValue.setText(companyDetails.getString("HeadRValue"));
        lValue.setTextColor(companyDetails.getInt("LColor"));
        rValue.setTextColor(companyDetails.getInt("RColor"));
    }


}
