package com.nri.trading.oms.ui.main.marketwatch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.nri.trading.R;
import com.nri.trading.oms.data.model.MarketWatchDetailsData;
import com.nri.trading.oms.data.model.StickViewDetailItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by rahulc on 20.1.16.
 */

public class MarketWatchDetailsListAdapter extends RecyclerSwipeAdapter<MarketWatchDetailsListAdapter.MarketWatchDetailsViewHolder> {

    private LayoutInflater layoutInflater;
    ViewGroup viewGroup;
    List<MarketWatchDetailsData> marketWatchDetailList = Collections.emptyList();
    Context context;
    BottomSheetLayout bottomSheetLayout;

    public MarketWatchDetailsListAdapter(Context context, List<MarketWatchDetailsData> detailList, BottomSheetLayout bottomSheetLayout) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        marketWatchDetailList = detailList;
        this.bottomSheetLayout = bottomSheetLayout;
    }


    @Override
    public MarketWatchDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = layoutInflater.inflate(R.layout.market_watch_details_list, parent, false);
        MarketWatchDetailsViewHolder marketWatchDetailsViewHolder = new MarketWatchDetailsViewHolder(myView);
        viewGroup = parent;
        return marketWatchDetailsViewHolder;
    }


    @Override
    public void onBindViewHolder(final MarketWatchDetailsViewHolder holder, int position) {
        final MarketWatchDetailsData currentCompanyHistoryData = marketWatchDetailList.get(position);
        holder.eoCompanyName.setText(currentCompanyHistoryData.getEoCompanyName());
        holder.eoHeadValue.setText(currentCompanyHistoryData.getEoHeadValue());
        holder.eoExpire.setText(currentCompanyHistoryData.getEoExpire());
        holder.eoStrike.setText(currentCompanyHistoryData.getEoStrike());
        holder.eoCall.setText(currentCompanyHistoryData.getEoCall());
        holder.eoSwipeHeadValue.setText(currentCompanyHistoryData.getEoHeadValue());

        double rValue = Double.parseDouble(currentCompanyHistoryData.getEoRValue());

        String rv;

        if (rValue > 0) {
            holder.eoRValue.setTextColor(Color.parseColor("#2e992e"));
            holder.eoSwipeRValue.setTextColor(Color.parseColor("#2e992e"));
            rv = "+" + rValue;
        } else {
            holder.eoRValue.setTextColor(Color.parseColor("#dc2323"));
            holder.eoSwipeRValue.setTextColor(Color.parseColor("#dc2323"));
            rv = String.valueOf(rValue);
        }
        holder.eoRValue.setText(rv);
        holder.eoSwipeRValue.setText(rv);

        double lValue = Double.parseDouble(currentCompanyHistoryData.getEoLValue());
        String lv = lValue + "%";
        if (lValue > 0) {
            holder.eoLValue.setTextColor(Color.parseColor("#2e992e"));
            holder.eoSwipeLValue.setTextColor(Color.parseColor("#2e992e"));
        } else {
            holder.eoLValue.setTextColor(Color.parseColor("#dc2323"));
            holder.eoSwipeLValue.setTextColor(Color.parseColor("#dc2323"));
        }
        holder.eoLValue.setText(lv);
        holder.eoSwipeLValue.setText(lv);

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wrapper1));
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, holder.swipeLayout.getFocusedChild());

        mItemManger.bindView(holder.itemView, position);

        holder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStockValue(holder);
            }
        });
    }

    private void setStockValue(MarketWatchDetailsViewHolder holder) {
        View parentView = (View) viewGroup.getParent();
        TextView shortName = (TextView) parentView.findViewById(R.id.eqOptionCompanyShortName);
        String ShortName = (String) shortName.getText();
        TextView headValue1, headValue2, headValue3, headValue4, headValue5, headValue6;
        ListView listView;
        final View stockView = layoutInflater.inflate(R.layout.stock_view_bottom_sheet, bottomSheetLayout, false);
        ImageButton stockClose = (ImageButton) stockView.findViewById(R.id.stockClose);
        stockClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetLayout.dismissSheet();
            }
        });
        headValue1 = (TextView) stockView.findViewById(R.id.stockCompanyName);
        headValue2 = (TextView) stockView.findViewById(R.id.stockHeadValue);
        headValue3 = (TextView) stockView.findViewById(R.id.StockCallText);
        headValue4 = (TextView) stockView.findViewById(R.id.stockCharge);
        headValue5 = (TextView) stockView.findViewById(R.id.stockExpire);
        headValue6 = (TextView) stockView.findViewById(R.id.stockStrike);
        listView = (ListView) stockView.findViewById(R.id.stockOptionList);
        headValue1.setText("(" + ShortName + ")" + holder.eoCompanyName.getText());
        headValue2.setText(holder.eoHeadValue.getText());
        headValue3.setText(holder.eoCall.getText());
        String lvalue = (String) holder.eoLValue.getText();
        lvalue = lvalue.replace("%", "");
        headValue4.setText(lvalue);
        headValue5.setText(holder.eoExpire.getText());
        headValue6.setText(holder.eoStrike.getText());
        StockViewAdapter stockViewAdapter = new StockViewAdapter(stockView.getContext(), getData());
        listView.setAdapter(stockViewAdapter);
        bottomSheetLayout.showWithSheetView(stockView);

    }

    public static List<StickViewDetailItem> getData() {
        List<StickViewDetailItem> stickViewDetailItems = new ArrayList<>();
        StickViewDetailItem stickViewDetailItem = new StickViewDetailItem();
        stickViewDetailItem.setLeftLabel("Last Price");
        stickViewDetailItem.setLeftValue("12.52");
        stickViewDetailItem.setRightLabel("Bid");
        stickViewDetailItem.setRightValue("15.25");
        for (int index = 1; index <= 5; index++)
            stickViewDetailItems.add(stickViewDetailItem);

        return stickViewDetailItems;
    }

    @Override
    public int getItemCount() {
        return marketWatchDetailList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    class MarketWatchDetailsViewHolder extends RecyclerView.ViewHolder {
        TextView eoCompanyName;
        TextView eoHeadValue;
        TextView eoRValue;
        TextView eoLValue;
        TextView eoExpire;
        TextView eoStrike;
        TextView eoCall;
        TextView eoSwipeHeadValue;
        TextView eoSwipeRValue;
        TextView eoSwipeLValue;
        Button eoSwipeSell, eoSwipeBuy;
        SwipeLayout swipeLayout;

        public MarketWatchDetailsViewHolder(View itemView) {
            super(itemView);
            eoCompanyName = (TextView) itemView.findViewById(R.id.eoCompanyName);
            eoHeadValue = (TextView) itemView.findViewById(R.id.eoHeadValue);
            eoRValue = (TextView) itemView.findViewById(R.id.eoRvalue);
            eoLValue = (TextView) itemView.findViewById(R.id.eoLvalue);
            eoExpire = (TextView) itemView.findViewById(R.id.eoExpire);
            eoStrike = (TextView) itemView.findViewById(R.id.eoStrike);
            eoCall = (TextView) itemView.findViewById(R.id.eoCall);
            eoSwipeHeadValue = (TextView) itemView.findViewById(R.id.eoSwipeHeadValue);
            eoSwipeRValue = (TextView) itemView.findViewById(R.id.eoSwipeRvalue);
            eoSwipeLValue = (TextView) itemView.findViewById(R.id.eoSwipeLvalue);
            eoSwipeSell = (Button) itemView.findViewById(R.id.eoSwipeSell);
            eoSwipeBuy = (Button) itemView.findViewById(R.id.eoSwipeBuy);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
        }

        /*@Override
        public void onClick(View view) {
            Toast.makeText(context, "I am Testing Position " + getAdapterPosition(), Toast.LENGTH_LONG).show();
            marketWatchDetailList.remove(getAdapterPosition());
            notifyItemRemoved(getAdapterPosition());
        }*/
    }

}
