package com.nri.trading.oms.ui.main.navigation.view;

import android.content.Context;
import android.util.Log;

import com.nri.trading.oms.model.MenuDTO;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by deor on 12-01-2016.
 */
public class NavigationViewImpl implements NavigationView {


    //Map<String, List<String>> expandableListData = new LinkedHashMap<>();
    @Override
    public void onUiLoad(Context context, boolean status, List<MenuDTO> userList , Map<String, List<String>> expandableListData) {
        expandableListData = new LinkedHashMap<>();
        List<MenuDTO> menuDTOs = new ArrayList<MenuDTO>();
        menuDTOs.addAll(userList);
        Log.d("Menu Size-----", String.valueOf(menuDTOs.size()));
        for (MenuDTO menuDTO : userList) {
            String resourceName = menuDTO.getNameKey();
            int resourceId = context.getResources().getIdentifier(resourceName, "string", context.getPackageName());
            if (menuDTO.getChildren() == null) {

                Log.d("*******Val", context.getResources().getString(resourceId));
                expandableListData.put(context.getResources().getString(resourceId), null);
            } else {
                List<String> subMenus = new ArrayList<String>();
                for (MenuDTO childMenu : menuDTO.getChildren()) {
                    subMenus.add(childMenu.getNameKey());
                }
                expandableListData.put(context.getResources().getString(resourceId), subMenus);
            }
        }

    }
   /* public Map<String, List<String>>  getMenuData(){
        return expandableListData;
    }*/
  //  Log.d("Exp List @@@@ " , expandableListData);
}
