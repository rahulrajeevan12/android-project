package com.nri.trading.oms.ui.main.marketwatch.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchTabFragment;

/**
 * Created by rahulc on 19.1.16.
 */
public class MarketWatchPagerAdapter extends FragmentPagerAdapter {

    public MarketWatchPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return new MarketWatchTabFragment();
    }


    @Override
    public int getCount() {

        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Equity Future";
            case 1:
                return "Equity Option";
            case 2:
                return "Index Future";
            case 3:
                return "Index Option";
        }
        return null;
    }
}
