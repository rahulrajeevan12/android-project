package com.nri.trading.oms.ui.main.navigation.presenter;

import android.content.Context;
import android.util.Log;

import com.nri.trading.oms.api.UicontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.MenuDTO;
import com.nri.trading.oms.ui.main.navigation.view.NavigationViewImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by deor on 12-01-2016.
 */
public class NavigationPresenterImpl implements NavigationPresenter {
    static List<MenuDTO> menuDTOs = new ArrayList<MenuDTO>();
    NavigationViewImpl navigationView = new NavigationViewImpl();
    @Override
    public void getData(final Context context , final Map<String, List<String>> expandableListData) {

        DataClient dataClient = new DataClient(context);
        dataClient.createService(UicontrollerApi.class).getMenuUsingGET("p", new Callback<List<MenuDTO>>() {
            @Override
            public void success(List<MenuDTO> userList, Response response) {
                navigationView.onUiLoad(context,true, userList,expandableListData );
            }
            @Override
            public void failure(RetrofitError error) {
                Log.d("Erooor::::::::::", error.toString());
            }
        });

    }

}
