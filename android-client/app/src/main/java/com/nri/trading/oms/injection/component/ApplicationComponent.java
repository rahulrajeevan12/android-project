/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */

package com.nri.trading.oms.injection.component;


import android.app.Application;
import android.content.Context;

import com.nri.trading.oms.OmsApplication;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.injection.ApplicationContext;
import com.nri.trading.oms.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules =
        {
                ApplicationModule.class
       }
)
public interface ApplicationComponent {

    void inject(OmsApplication omsApplication);

    @ApplicationContext Context context();
    Application application();
    DataClient dataClient();

    //PreferencesHelper preferencesHelper();
   //DatabaseHelper databaseHelper();
}
