/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */

package com.nri.trading.oms.data.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.content.Context;

import com.nri.trading.oms.Constants;
import com.nri.trading.oms.data.local.PreferencesHelper;

import javax.inject.Inject;

import io.swagger.client.ApiClient;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by rahulc on 7.1.16.
 */
public class DataClient extends ApiClient {
    Context aContext;


    public DataClient(Context context) {
        super();
        aContext = context;
    }


    @Override
    public void createDefaultAdapter() {
        Gson gson = (new GsonBuilder()).setDateFormat(Constants.GSON_DATE_FORMAT).create();
        setAdapterBuilder(new RestAdapter.Builder().setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                //if(NetworkUtil.isUserLoggedIn(aContext)) {
                    request.addHeader("Authorization", PreferencesHelper.getSharedPreferenceString(aContext, Constants.JWT_TOKEN_KEY));
                //}
            }
        }).setEndpoint(Constants.REST_END_POINT).setConverter(new GsonConverterWrapper(gson)));
    }


}
