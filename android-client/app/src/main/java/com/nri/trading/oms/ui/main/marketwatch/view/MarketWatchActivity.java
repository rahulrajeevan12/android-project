package com.nri.trading.oms.ui.main.marketwatch.view;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.nri.trading.R;
import com.nri.trading.oms.ui.main.marketwatch.adapter.MarketWatchPagerAdapter;
import com.nri.trading.oms.ui.main.navigation.view.NavigationActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class MarketWatchActivity extends NavigationActivity {
    private MarketWatchPagerAdapter mMarketWatchPagerAdapter;


    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DrawerLayout mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ButterKnife.bind(this);

        //Inflate your activity layout
        View contentView = inflater.inflate(R.layout.activity_market_watch, null, false);
        mDrawer.addView(contentView, 0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.market_watch_toolbar);
        setSupportActionBar(toolbar);

        mMarketWatchPagerAdapter = new MarketWatchPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager_market_watch);
        mViewPager.setAdapter(mMarketWatchPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_market_watch);
        tabLayout.setupWithViewPager(mViewPager);

        Spinner spinner = (Spinner) findViewById(R.id.market_spinner);
        List<String> marketType = new ArrayList<String>();
        marketType.add("ASX");
        marketType.add("BSX");
        marketType.add("CSX");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>
                (this, android.R.layout.simple_spinner_item,marketType);

        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);



    }

}
