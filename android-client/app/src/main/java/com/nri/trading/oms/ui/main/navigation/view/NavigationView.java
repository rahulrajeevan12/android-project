package com.nri.trading.oms.ui.main.navigation.view;

import android.content.Context;

import com.nri.trading.oms.model.MenuDTO;

import java.util.List;
import java.util.Map;

/**
 * Created by deor on 12-01-2016.
 */
public interface NavigationView {
    void onUiLoad(final Context context , boolean status , List<MenuDTO> userList , Map<String, List<String>> expandableListData);

}
