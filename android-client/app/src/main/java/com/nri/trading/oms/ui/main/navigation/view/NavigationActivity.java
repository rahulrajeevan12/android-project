/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */

package com.nri.trading.oms.ui.main.navigation.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.nri.trading.R;
import com.nri.trading.oms.api.UicontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.MenuDTO;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.navigation.adapter.CustomExpandableListAdapter;
import com.nri.trading.oms.ui.main.order.entry.OrderEntryActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by deor on 12-01-2016.
 */
public class NavigationActivity extends BaseActivity {

    @Inject
    DataClient mDataClient;

    @Bind(R.id.drawer_layout)
    protected DrawerLayout mDrawerLayout;
    @Bind(R.id.navList)
    ExpandableListView mExpandableListView;

    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    private ExpandableListAdapter mExpandableListAdapter;
    private List<String> mExpandableListTitle;
    private Map<String, List<String>> mExpandableListData;

    private  Map<String,String> activityMap;

    private int lastExpandedPosition = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        activityComponent().inject(this);
        ButterKnife.bind(this);
        // As we're using a Toolbar, we should retrieve it and set it
        // to be our ActionBar
      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.status_bar_color)));*/
        // Now retrieve the DrawerLayout so that we can set the status bar color.
        // This only takes effect on Lollipop, or when using translucentStatusBar
        // on KitKat.
        //drawerLayout.setStatusBarBackgroundColor(getResources().getColor());

        mActivityTitle = getTitle().toString();
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.nav_header, null, false);
        mExpandableListView.addHeaderView(listHeaderView);
        setupDrawer();
        mDataClient.createService(UicontrollerApi.class).getMenuUsingGET("", new Callback<List<MenuDTO>>() {
            @Override
            public void success(List<MenuDTO> userList, Response response) {
                mExpandableListData = new LinkedHashMap<>();
                activityMap = new HashMap<String, String>();
                List<MenuDTO> menuDTOs = new ArrayList<MenuDTO>();
                menuDTOs.addAll(userList);
                for (MenuDTO menuDTO : userList) {
                    String resourceName = menuDTO.getNameKey();
                    int resourceId = getResources().getIdentifier(resourceName, "string", getPackageName());
                    if (menuDTO.getChildren() == null) {
                        mExpandableListData.put(getResources().getString(resourceId), new ArrayList<String>());
                        //TODO url mapping
                        activityMap.put(getResources().getString(resourceId) , menuDTO.getUrl());
                       // if(menuDTO.getStyleClass() == null)
                    } else {
                        List<String> subMenus = new ArrayList<String>();
                        for (MenuDTO childMenu : menuDTO.getChildren()) {
                            subMenus.add(childMenu.getNameKey());
                        }
                        mExpandableListData.put(getResources().getString(resourceId), subMenus);
                    }
                }
                mExpandableListTitle = new ArrayList(mExpandableListData.keySet());
                addDrawerItems();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("Erooor::::::::::", error.toString());
            }
        });
    }

    private void addDrawerItems() {
        mExpandableListAdapter = new CustomExpandableListAdapter(this, mExpandableListTitle, mExpandableListData);
        mExpandableListView.setAdapter(mExpandableListAdapter);
        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                getSupportActionBar().setTitle(mExpandableListTitle.get(groupPosition).toString());
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    mExpandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });


        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                String selectedItem = mExpandableListTitle.get(groupPosition).toString();
                Log.d("Group view ::::::::",mExpandableListTitle.get(groupPosition).toString());
                if(((List) (mExpandableListData.get(mExpandableListTitle.get(groupPosition)))).size() == 0 ) {
                    switch (selectedItem) {
                        case "Market Watch":
                            mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                }
                return false;
            }

        });
        mExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
               // getSupportActionBar().setTitle(R.string.navigation);
               // mSelectedItemView.setText(R.string.selected_item);
            }
        });
        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                String selectedItem = ((List) (mExpandableListData.get(mExpandableListTitle.get(groupPosition))))
                        .get(childPosition).toString();

                switch (selectedItem){
                    case "trading.oms.menu.orderentry" : Intent orderEntryIntent = new Intent(NavigationActivity.this, OrderEntryActivity.class);
                                                         startActivity(orderEntryIntent);

                }
               // getSupportActionBar().setTitle(selectedItem);
                //mSelectedItemView.setText(mExpandableListTitle.get(groupPosition).toString() + " -> " + selectedItem);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }
    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle(R.string.navigation);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_navigation);
        mDrawerToggle.onConfigurationChanged(config);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.locale_en){
            changeLocale("en_US");
        }
        if(id == R .id.locale_jp){
            changeLocale("ja");
        }

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mDrawerLayout.closeDrawers();
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    private void  changeLocale(String language){
        if(getResources().getConfiguration().locale.toString().equals(language)){
            return;
        }
        Locale locale = null;
        locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_navigation);
        restartActivity();
    }
}
