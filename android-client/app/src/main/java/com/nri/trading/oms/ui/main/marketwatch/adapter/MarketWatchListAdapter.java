package com.nri.trading.oms.ui.main.marketwatch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.nri.trading.R;
import com.nri.trading.oms.data.model.MarketWatchMainData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahulc on 19.1.16.
 */
public class MarketWatchListAdapter extends BaseAdapter implements Filterable {

    private LayoutInflater inflater;
    List<MarketWatchMainData> marketWatchDataList = new ArrayList<>();
    List<MarketWatchMainData> mDisplayedValues;
    Context context;

    public MarketWatchListAdapter(Context context, List<MarketWatchMainData> marketWatchMainData) {
        this.mDisplayedValues = marketWatchMainData;
        this.context = context;
        this.marketWatchDataList = marketWatchMainData;
        inflater = LayoutInflater.from(this.context);

    }

    @Override
    public int getCount() {
        return mDisplayedValues.size();
    }

    @Override
    public MarketWatchMainData getItem(int i) {
        return mDisplayedValues.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, final ViewGroup viewGroup) {
        final MarketWatchViewHolder marketWatchViewHolder;

        if (view == null) {
            view = inflater.inflate(R.layout.market_watch_main_list, viewGroup, false);
            marketWatchViewHolder = new MarketWatchViewHolder(view);
            view.setTag(marketWatchViewHolder);
        } else {
            marketWatchViewHolder = (MarketWatchViewHolder) view.getTag();
        }

        final MarketWatchMainData currentData = getItem(i);

        marketWatchViewHolder.companyName.setText(currentData.getCompanyName());
        marketWatchViewHolder.companyShortName.setText(currentData.getCompanyShortName());
        marketWatchViewHolder.headValue.setText(currentData.getHeadValue());

        double lValue = Double.parseDouble(currentData.getHeadLeftValue());
        String lv = lValue + "%";
        marketWatchViewHolder.lValue.setText(lv);
        if (lValue > 0) {
            marketWatchViewHolder.lValue.setTextColor(Color.parseColor("#2e992e"));
        } else {
            marketWatchViewHolder.lValue.setTextColor(Color.parseColor("#dc2323"));
        }
        double rValue = Double.parseDouble(currentData.getHeadRightValue());
        String rv;

        if (rValue > 0) {
            marketWatchViewHolder.rValue.setTextColor(Color.parseColor("#2e992e"));
            rv = "+" + rValue;
        } else {
            marketWatchViewHolder.rValue.setTextColor(Color.parseColor("#dc2323"));
            rv = String.valueOf(rValue);
        }
        marketWatchViewHolder.rValue.setText(rv);
        return view;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();
                ArrayList<MarketWatchMainData> filteredArrList = new ArrayList<MarketWatchMainData>();
                if (marketWatchDataList == null) {
                    marketWatchDataList = new ArrayList<MarketWatchMainData>(mDisplayedValues);
                }
                if (charSequence == null || charSequence.length() == 0) {
                    results.count = marketWatchDataList.size();
                    results.values = marketWatchDataList;
                } else {
                    charSequence = charSequence.toString().toLowerCase();
                    for (int i = 0; i < marketWatchDataList.size(); i++) {
                        String data = marketWatchDataList.get(i).getCompanyName();
                        if (data.toLowerCase().startsWith(charSequence.toString())) {
                            MarketWatchMainData marketWatchMainData = new MarketWatchMainData();
                            marketWatchMainData.setCompanyName(marketWatchDataList.get(i).getCompanyName());
                            marketWatchMainData.setCompanyShortName(marketWatchDataList.get(i).getCompanyShortName());
                            marketWatchMainData.setHeadValue(marketWatchDataList.get(i).getHeadValue());
                            marketWatchMainData.setHeadLeftValue(marketWatchDataList.get(i).getHeadLeftValue());
                            marketWatchMainData.setHeadRightValue(marketWatchDataList.get(i).getHeadRightValue());
                            filteredArrList.add(marketWatchMainData);
                        }
                    }
                    // set the Filtered result to return
                    results.count = filteredArrList.size();
                    results.values = filteredArrList;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDisplayedValues = (ArrayList<MarketWatchMainData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    private class MarketWatchViewHolder {
        TextView companyName;
        TextView companyShortName;
        TextView headValue;
        TextView lValue;
        TextView rValue;

        public MarketWatchViewHolder(View view) {
            companyName = (TextView) view.findViewById(R.id.companyName);
            companyShortName = (TextView) view.findViewById(R.id.companyShortName);
            headValue = (TextView) view.findViewById(R.id.headValue);
            lValue = (TextView) view.findViewById(R.id.headLeftValue);
            rValue = (TextView) view.findViewById(R.id.headRightValue);
        }

    }
}
