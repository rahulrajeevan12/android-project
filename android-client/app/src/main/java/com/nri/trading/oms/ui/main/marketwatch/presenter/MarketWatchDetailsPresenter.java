package com.nri.trading.oms.ui.main.marketwatch.presenter;

import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchDetailsView;

import javax.inject.Inject;

/**
 * Created by rahulc on 20.1.16.
 */
public class MarketWatchDetailsPresenter implements Presenter<MarketWatchDetailsView> {
    MarketWatchDetailsView marketWatchDetailsView ;
    @Inject
    DataClient mDataClient;

    @Inject
    public MarketWatchDetailsPresenter() {
    }


    @Override
    public void attachView(MarketWatchDetailsView mvpView) {
        this.marketWatchDetailsView = mvpView;
    }

    @Override
    public void detachView() {
        this.marketWatchDetailsView = null;
    }

    public void getExchangeList(){

    }

    public void getMarketDataList(){

    }

}
