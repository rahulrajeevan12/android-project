/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */

package com.nri.trading.oms.ui.main.login.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nri.trading.R;
import com.nri.trading.oms.Constants;
import com.nri.trading.oms.data.local.PreferencesHelper;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.login.presenter.LoginPresenter;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchActivity;
import com.nri.trading.oms.util.NetworkUtil;
import com.nri.trading.oms.util.ViewUtil;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.RetrofitError;


public class LoginActivity extends BaseActivity implements LoginView {

    @Inject
    LoginPresenter mLoginPresenter;

    // UI references.
    private View mProgressView;
    private View mLoginFormView;
    @Bind(R.id.edit_user_name)
    EditText mUserName;
    @Bind(R.id.edit_password)
    EditText mPassword;
    @Bind(R.id.button_login)
    Button mLoginButton;
    Context context;

    Boolean validLength = false;

    private static final int CREDENTIALS_MIN_LENGTH = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.activity_login);
        activityComponent().inject(this);
        ButterKnife.bind(this);
        mLoginPresenter.attachView(this);


        mLoginButton.setEnabled(false);

        /*Adds a TextWatcher listener to listen text changes*/
        mUserName.addTextChangedListener(mLoginInputWatcher);
        mPassword.addTextChangedListener(mLoginInputWatcher);

        /*Handle the IME_ACTION of the keyboard*/
        mPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });


        mLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtil.isNetworkConnected(getApplicationContext()))
                    attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }


    private void attemptLogin() {
        String userName = mUserName.getText().toString();
        String password = mPassword.getText().toString();
        mLoginPresenter.doLogin(userName, password);
        showProgress(true);
        ViewUtil.hideKeyboard(this);
    }

    private TextWatcher mLoginInputWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (mUserName.getText().toString().length() >= CREDENTIALS_MIN_LENGTH && mPassword.getText().toString().length() >= CREDENTIALS_MIN_LENGTH) {
                mLoginButton.setEnabled(true);
            } else {
                mLoginButton.setEnabled(false);
            }
        }
    };


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void showProgress(final boolean show) {
        /* On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
         for very easy animations. If available, use these APIs to fade-in
         the progress spinner.*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            /*The ViewPropertyAnimator APIs are not available, so simply show
             and hide the relevant UI components.*/
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public void onLoginSuccess(boolean status, String response) {
        if (status == true) {
            PreferencesHelper.setSharedPreferenceString(context, Constants.JWT_TOKEN_KEY, response);
            PreferencesHelper.setSharedPreferenceString(context, Constants.USER_LOGGED_IN_KEY, Constants.TRUE);
            Intent marketWatchActivity = new Intent(getApplicationContext(), MarketWatchActivity.class);
            startActivity(marketWatchActivity);
            finish();
            showProgress(false);

        } else {
            showProgress(false);
        }
    }

    @Override
    public void onLoginFailure(RetrofitError error) {
        //DialogFactory.createSimpleOkErrorDialog(context,"Invalid credentials","Invalid User name or password").show();
        Toast.makeText(LoginActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();
        showProgress(false);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginPresenter.detachView();
    }

}

