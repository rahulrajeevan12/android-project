package com.nri.trading.oms.ui.main.marketwatch.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nri.trading.R;
import com.nri.trading.oms.data.model.StickViewDetailItem;

import java.util.List;

/**
 * Created by rahulc on 25.1.16.
 */
public class StockViewAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    List<StickViewDetailItem> mListValues;
    Context context;

    public StockViewAdapter(Context context, List<StickViewDetailItem> mListValues) {
        this.context = context;
        this.mListValues = mListValues;
        inflater = LayoutInflater.from(this.context);
    }

    @Override

    public int getCount() {
        return mListValues.size();
    }

    @Override
    public StickViewDetailItem getItem(int i) {
        return mListValues.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final StockViewHolder stockViewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.stock_details_layout, viewGroup, false);
            stockViewHolder = new StockViewHolder(view);
            view.setTag(stockViewHolder);
        } else {
            stockViewHolder = (StockViewHolder) view.getTag();
        }
        final StickViewDetailItem currentData = getItem(i);
        stockViewHolder.leftLabel.setText(currentData.getLeftLabel());
        stockViewHolder.leftValue.setText(currentData.getLeftValue());
        stockViewHolder.rightLabel.setText(currentData.getRightLabel());
        stockViewHolder.rightValue.setText(currentData.getRightValue());
        return view;
    }

    private class StockViewHolder {
        TextView leftLabel;
        TextView leftValue;
        TextView rightLabel;
        TextView rightValue;

        public StockViewHolder(View view) {
            leftLabel = (TextView) view.findViewById(R.id.text_left_label);
            leftValue = (TextView) view.findViewById(R.id.text_left_value);
            rightLabel = (TextView) view.findViewById(R.id.text_right_label);
            rightValue = (TextView) view.findViewById(R.id.text_right_value);
        }
    }
}
